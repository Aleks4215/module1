﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 module1 = new Module1();
            module1.SwapItems(3, 4);
            int[] input = { 4, 556, 77, 87, 3 };
            module1.GetMinimumValue(input);
        }


        public int[] SwapItems(int a, int b)
        {
            var swapedItems = new int[2];
            a = a + b;
            b = b - a;
            b = - b;
            a = a - b;
            swapedItems[0] = a;
            swapedItems[1] = b;
            return swapedItems;
        }

        public int GetMinimumValue(int[] input)
        {

            return input.Min();
        }
    }
}
